<!--
vim: tw=70
SPDX-License-Identifier: (CC-BY-SA-4.0 OR GFDL-1.3-or-later)
Copyright 2019 Nick Howell
-->

# Collaboration II

[back](../)

<div style="column-width:30em">

## Topics
* Patch review.
* Rebasing, merging, reverting.
* Filters and hooks.

## Reading
todo

</div>

