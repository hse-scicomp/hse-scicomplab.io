<!--
vim: tw=70
SPDX-License-Identifier: (CC-BY-SA-4.0 OR GFDL-1.3-or-later)
Copyright 2019 Nick Howell
-->

# Licensing and redistribution

[back](../)

<div style="column-width:30em">

## Topics
* [Licensing and copyright](../notes/05-licensing/05.01-intro.html)
* [Reproducibility](../notes/05-licensing/05.02-reprod.html)
* [Code](../notes/05-licensing/05.03-code.html)
* [Art](../notes/05-licensing/05.04-art.html)
* [Data](../notes/05-licensing/05.05-data.html)

## Activity/Practical

Write a *data policy* for your НИС. (You can submit one per НИС, if
you work on it together.) This should include:
* a description of what data you will use
* where that data comes from, and what license it is under
* (roughly) what you will do with the data
* whether you can/will release the data, and under what license
* how another research group will be able to reproduce your project

</div>

