<!--
vim: tw=70
SPDX-License-Identifier: (CC-BY-SA-4.0 OR GFDL-1.3-or-later)
Copyright 2019 Nick Howell
-->

# Collaboration I

[back](../)

<div style="column-width:30em">

## Topics
* [Collaboration](../notes/04-collaboration1/04.00-collab.html)
* [Communications channels](../notes/04-collaboration1/04.01-channels.html)
* [Version control systems](../notes/04-collaboration1/04.02-vcs.html)
* [Quality control](../notes/04-collaboration1/04.03-qc.html)

## Activities / Practical
* [git tutorial](../practicals/04-git.html)

</div>

