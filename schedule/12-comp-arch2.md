<!--
vim: tw=70
SPDX-License-Identifier: (CC-BY-SA-4.0 OR GFDL-1.3-or-later)
Copyright 2019 Nick Howell
-->

# Computer Architecture II

[back](../)

<div style="column-width:30em">

## Topics
* Heterogeneous computing, cluster computing, distributed
  computing.
* Hard vs easy parallelism.

## Reading
todo

</div>

