
<!--
vim: tw=70
SPDX-License-Identifier: (CC-BY-SA-4.0 OR GFDL-1.3-or-later)
Copyright 2019 Nick Howell
-->

# Data Representation I

[back](../)

<div style="column-width:30em">

## Topics
* [Text encodings and Unicodes](../notes/02-data-rep1/02.01-text.html)
* [Numerical representations](../notes/02-data-rep1/02.02-numbers.html)
* [Simple data structures: lists, trees, graphs](../notes/02-data-rep1/02.03-structures.html)
* [Cryptographic ideas](../notes/02-data-rep1/02.04-cryptography.html)

## Activity
* Install [gnupg]
* Using [PGP tutorial](../practicals/02-pgp.html):
  * Send me a PGP-encrypted e-mail (my key is
    [fpr:A59F263093CB492B4479EDAE6E855DEE48E8A9D1])
  * Send me a PGP-signed e-mail
  * Send me an e-mail with your PGP public key
  * Add your PGP public key to your GitHub/GitLab account

## Quick tutorials
* [gnupg quickstart](../practicals/02-gpg.html)
* [reddit guide to
  kleopatra](https://www.reddit.com/r/GPGpractice/wiki/the_process/kleopatra) (sorry, this is the best I could find)
* EFF surveilance self-defense guide (possibly out-of-date!):
  [Linux](https://ssd.eff.org/en/module/how-use-pgp-linux)
  [MacOS](https://ssd.eff.org/en/module/how-use-pgp-mac-os-x)
  [Windows](https://ssd.eff.org/en/module/how-use-pgp-windows)

[gnupg]: https://www.gnupg.org/
[fpr:A59F263093CB492B4479EDAE6E855DEE48E8A9D1]: ../A59F263093CB492B4479EDAE6E855DEE48E8A9D1.asc
</div>

