<!--
vim: tw=70
SPDX-License-Identifier: (CC-BY-SA-4.0 OR GFDL-1.3-or-later)
Copyright 2019 Nick Howell
-->

# Software Packaging I

[back](../)

<div style="column-width:30em">

## Topics
* User-local / system-global installation.
* Managed vs unmanaged installs.
* Package management: distributions, languages, user.
* Image-based: VMs, containers.

## Reading
todo

</div>

