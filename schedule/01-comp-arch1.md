<!--
vim: tw=70
SPDX-License-Identifier: (CC-BY-SA-4.0 OR GFDL-1.3-or-later)
Copyright 2019 Nick Howell
-->

# Computer Architecture I

[back](../)

<div style="column-width:30em">

## Topics
* Ideas of electronics, physical components of the computer.
* Software, and software layering in modern multi-user
  computers (kernel, system, user software).
* Contrast against unikernel (less layering) and against
  highly virtualised (more layering) systems.
* Networking of various reliability/bandwidth/latency levels.

## Reading
<!--
ex:
r !echo ../notes/$(basename % .md)/* | xargs -n1 echo | while read l; do grep -m1 '\#' "$l" | tr '\#' '*' | sed -e 's/^* /* [/' -e "s@\$@](${l\%.md}.html)@"; done
-->
* [Computer architecture](../notes/01-comp-arch1/01.00-computer-arch.html)
* [Ideas of electronics](../notes/01-comp-arch1/01.01-electronics.html)
* [Physical make-up of a computer](../notes/01-comp-arch1/01.02-physical-components.html)
* [Software layering ](../notes/01-comp-arch1/01.03-software-layering.html)
* [Networking](../notes/01-comp-arch1/01.04-networking.html)

## Activities / Practical

* Install a Secure Shell (ssh) client; popular ones are [Putty] \(for
  Windows <10) and [OpenSSH] \(for \*nix and WSL). Use ssh to connect
  to the local machine (address, username, password given in-class),
  and create a file named after your e-mail address. (E.g.
  "`nlhowell@gmail.com`")

* Send an e-mail to
  `~nlhowell/hse-scicomp-21-announce+subscribe@lists.sr.ht`.

  This subscribes you to `~nlhowell/hse-scicomp-21-announce@lists.sr.ht`
  (announcement mailing list).

* Send an e-mail to `~nlhowell/hse-scicomp-21-submit@lists.sr.ht`.

  This is a send-only mailing list; you will not receive any e-mails.
  This is how you will submit reports and assessments.

  If you receive a message "unable to deliver … because it contains
  HTML", you need to find an e-mail client willing to send
  plain-text-only messages.

  Try "settings" on your mail-client or install a new one! [Mozilla
  Thunderbird] is pretty good for graphical mail clients.

[Putty]: https://putty.org
[OpenSSH]: https://www.openssh.com
[Mozilla Thunderbird]: https://www.thunderbird.net

</div>

