<!--
vim: tw=70
SPDX-License-Identifier: (CC-BY-SA-4.0 OR GFDL-1.3-or-later)
Copyright 2019 Nick Howell
-->

# Interfaces I

[back](../)

<div style="column-width:30em">

## Topics
* Command-line and text interfaces.
* Historical perspective.
* Teletypes, shells, Read-Evaluate-Print Loops.
* Basic \*nix-style conventions for interaction.
* Basics of shell scripting.
* Processes and job control.
* Quick intro to SSH?, text editors, GnuPG?, Git?

## Reading
* [Command-line and text interfaces](../notes/03-interfaces1/03.01-cli.html)
* [Using a CLI](../notes/03-interfaces1/03.02-repl.html)
* [Editing Files](../notes/03-interfaces1/03.03-editors.html)
* [Processes and job control](../notes/03-interfaces1/03.04-tasks.html)
* [Shell scripting](../notes/03-interfaces1/03.05-scripting.html)
* [Makefiles](../notes/03-interfaces1/03.06-makefiles.html)

## Practical
* complete [Unicode Unix for Poets], on your home \*nix machine (Mac
  may have subtle problems; WSL or other Unix is fine)

[Unicode Unix for Poets]: https://ftyers.github.io/079-osnov-programm/classes/01.html

</div>

