<!--
vim: tw=70
SPDX-License-Identifier: (CC-BY-SA-4.0 OR GFDL-1.3-or-later)
Copyright 2019 Nick Howell
-->

# Data representation/Interfaces II

[back](../)

<div style="column-width:30em">

## Topics
* Distributed data structures.
* Databases.
* Schedulers and cluster management.

## Reading
todo

</div>

