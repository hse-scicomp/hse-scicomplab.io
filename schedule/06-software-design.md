<!--
vim: tw=70
SPDX-License-Identifier: (CC-BY-SA-4.0 OR GFDL-1.3-or-later)
Copyright 2019 Nick Howell
-->

# Software Design Patterns

[back](../)

<div style="column-width:30em">

## Reading
* [Software design] - abstraction, tight- and loose-coupling.

[Software design]: ../notes/06-design/06.00-design.html

## Practical

[modular statistics collection](../practicals/06-stats.html)



</div>

