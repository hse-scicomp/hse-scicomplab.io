<!--
vim: tw=70
SPDX-License-Identifier: (CC-BY-SA-4.0 OR GFDL-1.3-or-later)
Copyright 2019 Nick Howell
-->

# Software Creation I

[back](../)

<div style="column-width:30em">

## Topics
* Compilers and assembly.
* Libraries and portability, linking and build.
* Interpreters.

## Reading
todo

</div>

