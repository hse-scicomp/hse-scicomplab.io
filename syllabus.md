
<!--
vim: tw=70
SPDX-License-Identifier: (CC-BY-SA-4.0 OR GFDL-1.3-or-later)
Copyright 2021 Nick Howell
-->

# Syllabus

[back](./)

<div style="column-width:30em">

## Course Prereqs
Students are expected to have a computer they can install software on,
and at least intermittent internet access. Access to e-mail is
mandatory; all other communication forms are optional. No knowledge of
computer science or engineering is expected.

Basic knowledge of boolean logic and arithmetic will be assumed; a
review of boolean logic will be given in the first lesson.

Basic knowledge of physics and chemistry will be helpful, but is not
required for successful participation in the course.

## Abstract
An introduction to the use of computers for the purpose of performing
scientific research. This class has three themes: understand computers
as scientific instruments, understand how science is performed with
computers, and understand how to effectively communicate science done
with computers. Starting from the physical and mathematical principles
underlying the functioning of computers, we will survey the fields of
computer science, hardware engineering, software design; we will gain
hands-on experience with modern scientific and open software design
techniques, as well as the tools which have been constructed to
support international collaboration on and presentation of these
systems.

## Learning Objectives
Students will become familiar with the internal workings of computing
systems, the ways in which computers are used to perform and
communicate scientific research, and become effective communicators
both of science and scientific results. Students will gain fluency in
the use of computers to collaborate on scientific research projects.

## Learning Outcomes
After this course, students will be able to:
* describe the components of a computer, and explain the basic
  functions of each from physics, hardware engineering, and software
  engineering perspectives
* explain the principles on which modern computing systems are
  designed
* explain the layering of software on a computer, and the principles
  of modular software design
* explain many elementary data structures useful in scientific work on
  computers
* explain the information flow in interaction with modern computer
  systems, including what processing is done at what layers
  communication protocols
* use with familiarity the tools and workflows common to open-source
  and scientific research to accomplish elementary tasks on computing
  systems
* effectively communicate science results and collaborate on
  scientfici research using computers

## Reading Assignments
There is no particular book for this course; I will be writing lecture
notes, and frequently these will come with links to additional reading
material.

## Grading
Grading will be based on practicals and written assignments. All
tasks are graded with maximum score 2; the rubrick is:
* 0 - no serious attempt to respond
* 1 - many or significant errors
* 2 - no more than a few minor errors

All assignments are weighted equally; your score for the class is your
average percentage over all homeworks and practicals.

### Practicals
Some weeks we will have a short lecture, and then a practical. The
practical will involve using a computer to accomplish some tasks; you
will afterwards write a report presenting your work, and submit this.

Reports should be written in full prose, in English (preferably) or
Russian (carefully); they should be submitted in plain-text formats
(LaTeX or Markdown). Results of programming can be submitted as
separate files, but the use of these should be demonstrated in the
report.

Note: I will only read the report. If the report asks me to run a
script, then I will look for the script and run it as directed by the
report. I will not go hunting for answers without prompting.

### Written Assignments
All weeks will have a short selection of reading assignments and a
short selection of written questions to answer. Weeks without
practicals will have more written questions than weeks without.

Answers will be shorter than reports, but should be written in the
same way: plain-text, full prose, with any programming submitted as
separate files and demonstrated in the answer text.

## Exams
During exam weeks, there will be slightly larger written assignments.
There will be no in-class exams; we will instead hold catch-up
sessions to finish practicals, ask questions, or I might lecture on
some additional topics.

</div>

