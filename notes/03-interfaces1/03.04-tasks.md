<!--
vim: tw=70
SPDX-License-Identifier: (CC-BY-SA-4.0 OR GFDL-1.3-or-later)
Copyright 2019 Nick Howell
-->

# Processes and job control

<div style="column-width:30em">

Read the section [System Architecture] from [Linux Sea]

[System Architecture]: http://swift.siphos.be/linux_sea/runninglinux.html#runninglinux_systemarchitecture
[Linux Sea]: http://swift.siphos.be/linux_sea

Read [Working with Processes] from [Linux Sea]. References to the
programs `eix-update` (to update the list of available packages) and
`emerge` (to upgrade installed software) can be ignored; these are
package management commands related to Gentoo. Replace with your own
package management commands.

[Working with Processes]: http://swift.siphos.be/linux_sea/processes.html

Read Fran Tyers' [Unicode Unix for Poets].

[Unicode Unix for Poets]: https://ftyers.github.io/079-osnov-programm/classes/01.html

</div>

