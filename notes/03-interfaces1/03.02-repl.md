<!--
vim: tw=70
SPDX-License-Identifier: (CC-BY-SA-4.0 OR GFDL-1.3-or-later)
Copyright 2019 Nick Howell
-->

# Using a CLI

<div style="column-width:30em">

## Glossary

We first present a quick glossary of terms.

a) Terminal: a piece of software that emulates the physical
telephone-typewriter. It runs a shell. Common terminals are "xfce
Terminal", "urxvt", "xterm", "konsole", "GNOME Terminal", "(Apple)
Terminal".

b) Shell: a call-response program that runs in the terminal. The shell
is a program that loops, prompting you to enter commands, and
displaying their output. Common shells are "bash", "zsh".

sample prompt:
```
nlhowell@mercury ~ $ _
                     ^ ---------- cursor, waiting for command

^^^^^^^^^^^^^^^^^^^^ ------------ prompt; has metadata
```

Prompts typically include username, hostname (the name of the computer
the shell is running on), the current working directory, and the
end-of-prompt delimiter $.

sample prompt-command-output:
```
nlhowell@mercury $ echo hi<RETURN>
hi
                          ^^^^^^^^ ---- <RETURN> executes command
                   ^^^^^^^ ------------ entered command
^^ ------------------------------------ output of the executed command
```

c) Command: a program, or pipeline of programs, together with their
arguments.

command with program "echo" and argument "hi"
```
$ echo hi
       ^^ ---- first (and only) argument
  ^^^^ ------- command name
^ ------------ prompt
```

pipeline of two subcommands: program "echo" with argument "hi", and
program "tr" with arguments "a-z" and "A-Z"; output of "echo" command
is fed as input to "tr" command
```
$ echo hi | tr a-z A-Z
HI
            ^^^^^^^^^^ ---- second command
          ^ --------------- pipe, connecting commands
  ^^^^^^^ ----------------- first command
  ^^^^^^^^^^^^^^^^^^^^ ---- pipeline consisting of two commands
```

d) Pipeline: a chain of programs (with arguments) which are hooked
together, so that the output of a program is fed to the next program
in the chain.

e) Arguments: a small collection of configuration data provided to a
program on the commandline.

## Reading command specifications

There is a special syntax for describing how to run a command: things
enclosed in \[square brackets\] are optional. An ascii ellipsis "..."
means "repeat".

Example: the `cat` program takes zero or more values for OPTION and
zero or more values for FILE. (The possible values for OPTION and FILE
are described in the help output.)
```
cat [OPTION]... [FILE]...
```

"Options" are small features which adjust how the program performs its
job.

Exercise: Compare the output of `ls` vs `ls -l` and `ls -a`; describe
how they are similar, and how they are different.

Some options take values: compare `seq 5` vs `seq -s , 5`

Some options have long and short names: `seq --separator=, 5`

## Navigating with the command-line

Read the following sections from [Using the Command Line] of [Linux Sea]:
* Navigating
* Listing Content
* Basic File Manipulation

[Using the Command Line]: http://swift.siphos.be/linux_sea/runninglinux.html#idm3548300267344
[Linux Sea]: http://swift.siphos.be/linux_sea

Read the `--help` output of the following commands:
* `ls`
* `cd` and `pwd`
* `echo`
* `seq`
* `cat`, `head`, and `tail`
* `pushd` and `popd`
* `cp` and `mv` and `rm`
* `mkdir` and `rmdir`
* `date`

Exercise: Explain `pushd` and `popd`; what data structure represents
your directory history? Give an example of using them to organise a
folder with music.

Exercise: Draw a partial tree of your filesystem, starting from the
children of your home directory. Include ancestors of your home
directory, and siblings of those ancestors. Exclude files, just show
directories. Here is mine:
```
  (root)/
    |
    | - bin/
    | - boot/
    | - etc/
    | - dev/
    | - home/
    |     | - nlhowell/
    |     |      | - mail/
    |     |      | - media/
    |     |      | - passport/
    |     |      | - src/
    |     |      | - travel/
    |     |      | - usr/
    |     |      | - work/
    |     |
    |     | - spamd/
    |
    | - lib/
    | - lost+found/
    | - mnt/
    | - opt/
    | - proc/
    | - root/
    | - run/
    | - sbin/
    | - sys/
    | - tmp/
    | - usr/
    | - var/
```

</div>

