<!--
vim: tw=70
SPDX-License-Identifier: (CC-BY-SA-4.0 OR GFDL-1.3-or-later)
Copyright 2021 Nick Howell
-->

# Introduction to Scientific Computing

This is the (under-construction) home page of the [HSE] [School of
Linguistics] course [Introduction to Scientific Computing]. Here you
will find information about:
* instructors
* meetings (time, place, readings)

[Syllabus](syllabus.html) - basic information about the class.

To download a plain-text copy of this site, clone
[https://gitlab.com/hse-scicomp/hse-scicomp.gitlab.io.git](https://gitlab.com/hse-scicomp/hse-scicomp.gitlab.io.git).

<div style="column-width:30em">

## Instructors
I'm Nick Howell, assistant professor at the [School of Linguistics].
My PGP fingerprint is A59F2630 93CB492B 4479EDAE 6E855DEE 48E8A9D1 
([key](A59F263093CB492B4479EDAE6E855DEE48E8A9D1.asc)). You can reach
me by e-mail at nlhowell at gmail dot com.

## Topics

The group will meet every other Wednesday, from 18.10 - 21 (with a
10min break in-between), by Zoom.

Rough schedule:
1.	[Computer architecture I](schedule/01-comp-arch1.html)
2.	[Data representation I](schedule/02-data-rep1.html)
3.	[Interfaces I](schedule/03-interfaces1.html)
4.	[Collaboration I](schedule/04-collaboration1.html)
5.	[Licensing](schedule/05-licensing.html)
6.	[Software design patterns](schedule/06-software-design.html)
7.	Review                                            
8.	[Software creation I](schedule/08-software-creation1.html)
9.	[Software packaging I](schedule/09-software-packaging1.html)
10.	[Collaboration II](schedule/10-collaboration2.html)
11.	[Graphical and web interfaces](schedule/11-graphical-and-web-interfaces.html)
12.	[Computer architecture II](schedule/12-comp-arch2.html)
13.	[Software creation II](schedule/13-software-creation2.html)
14.	[Data representation/Interfaces II](schedule/14-data-rep-interfaces2.html)
15.	Review


</div>

[HSE]: https://hse.ru
[School of Linguistics]: https://ling.hse.ru
[Introduction to Scientific Computing]: https://hse-scicomp.gitlab.io
