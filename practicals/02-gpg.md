<!--
vim: tw=70
SPDX-License-Identifier: (CC-BY-SA-4.0 OR GFDL-1.3-or-later)
Copyright 2019 Nick Howell
-->
<div style="max-width: 40em; margin: 0 auto">

# gnupg summary

### <a name="Abstract">Abstract</a>
[#Abstract]: #Abstract

A quick summary of the use of the `gpg` command-line suite. A "PGP
identity" means a uid list, master key-pair, possible sub-key-pairs.
See the PGP tutorial for more.

### known identities

`gpg` maintains a database of identities, and a trust database (of how much
you trust each uid).

List known PGP identities (`<search-term>` and `--with-fingerprint` are optional):
```
gpg --with-fingerprint --list-keys <search-term>
```

List those PGP identities whose master secret key you possess (i.e.,
*your* PGP identities):

```
gpg --with-fingerprint --list-secret-keys <search-term>
```

### printing the contents of a public key

```
gpg --armor --export <fingerprint>
```

### importing a new public key

From a file:
```
gpg --import < $file
```

From clipboard (after `<RETURN>` gpg will wait for paste):
```
gpg --import

<PASTE>
<Ctrl-D>
```

### signing a message

Signature follows message:
```
gpg --clearsign

<Message>
<Ctrl-D>
```

Create a signature file, to be sent with the file (creates `$file.sig`):
```
gpg --detach-sign $file
```

### verifying a message

Verify a signature:

A clear-signed message:
```
gpg --verify

<SIGNED MESSAGE>
<Ctrl-D>
```

A signature file (`$file` and `$file.sig` must be together):
```
gpg --verify $file.sig
```

### encrypting a message


To encrypt to multiple recipients, add more `--recipient $recipient`.
The value of `$recipient` can be a fingerprint, e-mail, or name.
```
gpg --armor --encrypt --recipient $recipient

<Message>
<Ctrl-D>
```

### decrypting a message

From clipboard:
```
gpg --decrypt

<ENC MESSAGE>
<Ctrl-D>
```

From file:
```
gpg --decrypt $file
```

</div>

