<!--
vim: tw=70
SPDX-License-Identifier: (CC-BY-SA-4.0 OR GFDL-1.3-or-later)
Copyright 2019 Nick Howell
-->

# Paper writing

<div style="column-width:30em">

In this practical, we will explore some techniques for effective
paper-writing. We will only explore paper-writing from the industrial
(i.e., mass-production) perspective, which is typical for commercial
and academic scientific research. When someone is attempting to judge
your scientific output, this is what they will look at.

## General ideas

Paper-writing and scientific research are two separate, but connected,
activities. To be an effective scientist, each of them must influence
the other: the direction of your research should be partly determined
by what will make the most attractive paper, and the structure and
(more obviously) the content of your paper should be partly (but not
entirely!) determined by the results of your research.

From the mass-production perspective, successful papers are those
which are accepted to peer-reviewed, highly-cited journals. In order
to be accepted, you must have good research, but it also must be
well-presented. (In fact, probably the second is the only factor
*actually* necessary, unfortunately...)

What constitutes well-presented research is most objectively
determined by choosing your target journal, and looking at similar
papers which have been accepted.

## Literature search

In order to increase the likelihood of acceptance, you will want to
make sure you have not only familiarity with the previous work (the
"literature"), but also similar papers which have been accepted by
your target journal.

Go back through the last 10 or 20 papers presenting similar subject
matter. (If these don't exist, you probably want to choose a different
journal!) Reverse the process of "growing the paper": first write
brief summaries of each subsection, then summaries of sections, then
just the table of contents.

Identify features which are common between the papers, and features
which are different, and decide which of these make sense for your
paper.

## Growing a paper

My strategy for paper-writing is to first perform the literature
search, then "grow" the paper. This consists of several steps:
1. Write a list of sections
2. For each section, expand into subsections (this is the "skeleton")
3. For each subsection, write a summary
4. For each summary, expand into the actual subsection

Here is a typical result of the first step:
1. Introduction
2. Background
3. Prior work
4. Product
6. Evaluation
7. Future work
8. Concluding remarks

And refined with the second step:
1. Introduction
	* invitation
	* technical aspects
	* cultural aspects
	* reading guide
2. Background
	* product background
	* evaluation background
3. Prior work
	* (list of each prior work)
4. Product
	* (list of each piece)
6. Evaluation
	* (list of evaluation methods)
7. Future work
8. Concluding remarks

Notice that most of these subsections can be written without any new
science actually being conducted, and that most can be written
concurrently. This means that if you have several collaborators, you
can assign each subsection to a different person, ideally based on
expertise.

## LaTeX

Now we move on to the actual technical part of this discussion. LaTeX
is a highly successful document creation platform, which takes in a
source code and compiles to PDF. The job that LaTeX performs is called
*typesetting*, a process historically carried out by human designers.

LaTeX itself is free software, and because the source code is plain
text, collaborative authoring can be very efficient with the use of
`git`. The most popular distribution of TeX these days is
[tug.org/texlive](TeX Live). There are also free-of-payment services,
notably Overleaf, that will provide a web interface allowing you to
use their computers for document authoring.

The basic structure of a LaTeX document is text, together with
commands that start with a backslash '\\':
```latex
\documentclass{article}
\title{My First Article}
\author{Me!}

\begin{document}
\maketitle

Hello!

This is my first article!

Like markdown,
single
linebreaks
count as spaces.

But separate with a blank line,
and you get a new paragraph!

% comments start with %
% if you want a real %
write \%

\end{document}
```

To compile from the command-line, simply save the file, for example as
`first.tex`, and run:
```bash
$ xelatex first.tex
```

This will produce `first.pdf`, which you can view in your favourite
PDF viewer.

There are actually many different implementations of LaTeX; the two
most modern are XeLaTeX and LuaLaTeX. (The latter actually embeds the
Lua programming language, allowing you to use a Lua script to generate
parts of the TeX code for your document.)

### Mathematics
LaTeX is the universal standard for authoring mathematics articles; it
has an unrivaled automated equation typesetter:

```latex
Here is a snippet of LaTeX code with the fundamental theorem of
calculus:
\[ \frac{d}{dx}\,\int_0^x f(x)\,dx = f(x) \]

Here \(d/dx\) means ``derivative'' and \(\int_0^x - \, dx\) means
``integral from zero to \(x\)''; and Newton and Leibnitz's fundamental
realisations were that taking the derivative and integrating are
opposites, just like addition and subtraction.
```

On Wikipedia, all equations are originally entered in LaTeX; if you
place your mouse over an image of an equation, you will see the LaTeX
code.

**Exercise:** Find Pythagoras' theorem and copy the LaTeX code into
your document.

### Sections and tables of contents

You can use the `\\section{Title}` command to label the start of a
section. The command `\tableofcontents` will insert the table of
contents of your article.

A good start for an article, then, is to just write the skeleton in
the form of section entries:
```latex
\documentclass{article}
\title{My Second Article}
\author{Still me!}
\date{Some time ago, really}

\begin{document}

\begin{abstract}
This is really just a pretend document
\end{abstract}

\tableofcontents

\section{Introduction}
\section{Background}
\section{Prior work}
\section{Product}
\section{Evaluation}
\section{Future work}
\section{Concluding remarks}

\end{document}
```

In order to get the table of contents to present, you will have to run
the compilation twice! This is because on the first pass, TeX doesn't
know what sections you have; it records them in an auxiliary `.aux`
file, and on the second pass knows what to put. This is a common theme
for bibliographies, as well.

**Exercise**: Try also the `\\subsection{Title}` and `\\section*{Title}` commands; the first is self-explanatory, and the second omits the section number.

### Other features

LaTeX has an enormous number of built-in features, and an even more
enormous collection of add-on packages (some of which are even *more*
enormous).

**Exercise**: Using the [en.wikibooks.org/wiki/LaTeX](LaTeX Wikibook),
make a table and include a picture into your document. Add captions.

**Exercise**: Try out the `tikz` diagram drawing package.

**Exercise**: To aid in collaboration, split your LaTeX article into
multiple files, using `\\input{filename-without-.tex-extension}` to
include them into the main project file.

## Presenting numerical data

### Statistics

When you wish to present numerical data (for example, a statistical
morphological analysis technique tested over many languages), you want
to provide some analysis of the numbers. The most elementary thing to
do is to compute the mean and range, but this does not tell you much.

**Exercise**: What is the mean? How can you *quantitatively* (instead
of subjectively) identify the outlier? (Hint: measure the distance
from the mean in standard deviations.)
```python
[ 0.5, 0.6, 0.4, 0.3, 0.5, 0.4, 10 ]
```

What should be done with outliers? Many scientists decide to remove
them; this manipulates your data, and is subjective! A better solution
is to use *robust statistics*: the equivalent of mean-standard
deviation is median-median absolute deviation.

**Exercise**: Write a function in python for computing the median
absolute deviation using numpy functions.

### Significant figures

Compute the following:
```python
numpy.mean([1, 0, 0])
```

My copy of python displays the result as `0.333333333333333` with
standard deviation of `0.4714045207910317`. If my input data is not so
precise, however, then neither should be my statistics! Typical
rule-of-thumb is to display two "significant figures" (digits after
the beginning zeroes) for the error, and use the same precision for
mean and tabular presentations. In that case, we would present the
statistics as `0.33 ± 0.47`.

(If you have explicit estimates for the error in each measurement,
there are weighted versions of mean, standard deviation, median, and
median absolute deviation.)

**Exercise**: Present mean and standard deviation of the following
data following the above rule-of-thumb:
```python
numpy.array([10.0430708 ,  9.54162715,  9.78773657, 10.00369808,
10.17996121, 9.83482543,  8.53733062,  9.83444007,  9.30412989,
10.36065521])
```

## Presenting graphical data

There are a number of nice python packages for generating graphs. See
`seaborn` and the underlying `matplotlib`.

One crucial piece of data that should be reflected in plots is error
bars; the `matplotlib` function `fill_between` is excellent for
showing confidence regions.

**Exercise**: Graph the following data, using `fill_between` to show
the error region:
```python
x = numpy.array([0. , 0.5, 1. , 1.5, 2. , 2.5, 3. , 3.5, 4. , 4.5, 5.
, 5.5, 6. , 6.5, 7. , 7.5, 8. , 8.5, 9. , 9.5])

y = numpy.array([1.03459772e-01, 1.07037512e-01, 2.64057215e+00,
1.48435497e+00, 1.65635053e+00, 3.84044927e+00, 3.42994990e+00,
4.57499703e+00, 6.93887905e+00, 8.80663156e+00, 1.14987962e+01,
1.55678407e+01, 2.04627227e+01, 2.52882327e+01, 3.29772013e+01,
4.27467014e+01, 5.52146951e+01, 6.97640740e+01, 8.91793401e+01,
1.15646303e+02])
```

</div>
